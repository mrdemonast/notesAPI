require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const app = express();


const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/notesRouter');
const authMiddleware = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(cors());
app.use(morgan('tiny'));

app.use('/api', authRouter);
app.use('/api/users', authMiddleware, userRouter);
app.use('/api/notes', authMiddleware, noteRouter);

const PORT = +process.env.PORT;
const DB = process.env.DB;

const startServer = async () => {
  try {
    await mongoose.connect(DB);
    app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
  } catch (err) {
    console.log(`Error: ${err.message}`);
  }
};

startServer();
