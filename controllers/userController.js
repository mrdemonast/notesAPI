const User = require('../models/userModel');
const bcrypt = require('bcryptjs');

class UserController {
  async getUser(req, res) {
    try {
      const user = await User.findOne({
        username: req.user.username,
      });

      if (!user) {
        return res.status(400).send({message: 'No user'});
      }

      res.status(200).send({
        user: {
          _id: user._id,
          username: user.username,
          createdDate: user.createdAt,
        },
      });
    } catch (e) {
      res.status(500).send({message: 'Internal server error'});
    }
  }

  async deleteUser(req, res) {
    try {
      User.findOneAndRemove({_id: req.user.id}, (err) => {
        if (err) {
          res.status(400).send({message: 'Can not remove'});
          return;
        }

        res.status(200).send({message: 'Success'});
      });
    } catch (e) {
      res.status(500).send({message: 'Internal server error'});
    }
  }

  async changePassword(req, res) {
    try {
      const oldPassword = req.body.oldPassword;
      const newPassword = req.body.newPassword;
      const user = await User.findOne({
        username: req.user.username,
      });

      if (!user) {
        res.status(400).send({message: 'User was not found'});
        return;
      }

      const isPasswordEqual = await bcrypt.compare(oldPassword, user.password);

      if (!isPasswordEqual) {
        res.status(400).send({message: 'Incorrect password'});
        return;
      }

      user.password = await bcrypt.hash(newPassword, 7);
      await user.save();
    } catch (e) {
      res.status(500).send({message: 'Internal server error'});
      return;
    }
    res.status(200).send({message: 'Success'});
  }
}

module.exports = new UserController();
