const express = require('express');
const router = express.Router();
const {registrate, login} = require('../controllers/authController');

router.post('/auth/register', registrate);
router.post('/auth/login', login);

module.exports = router;
